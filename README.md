# Python Tests for school visitors to our office #

fix the code to make all tests pass

### What are the exercises for?  ###

The exercises are designed for the coding part of your visit, but 
feel free to download the repo if you want to have a look
at it later on.

The aim is to introduce you to test driven development.

All tests are currently failing, so you need to fix the code in the functions to make them pass.

### You only need to look at main.py and exercises.py  ###


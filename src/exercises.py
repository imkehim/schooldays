# --Starter-one--
def hello_world():
    return "Hello Worlld"


# --Starter-two--and--three--
def add_numbers(a, b):
    return 0


# --Starter-four--
def subtract_numbers(a, b):
    # your code
    return 0


# --Starter-five--
def multiply_numbers(c, d):
    return 0


# --Starter-six--
def divide_numbers(a, b):
    return 0


# --Starter-seven--and--eight--
def is_your_teacher_cool(teacher_is_cool):
    return ""


# for exercise eight think about adding an if / else statement to the function to make both tests pass


#  This stuff is a bit harder:

# --Starter-nine--
def tip_calculator(bill_amount):
    # tip should be calculated as 20 percent of the bill_amount
    # think about the maths operation that's needed to work out percentages
    # your code
    return 0


# --Starter-ten/eleven--
# Hint: my_string.split() will split a string into words
def first_word(string):
    return


# --Starter-twelve/thirteen--
# Hint: the len() function will give you the length of any list
def find_middle_element(list_of_names):
    return


# --Starter-fourteen/fifteen--
# Hint: a colon in array brackets omits one portion of a list like so: <full_list[:half_list]> - this returns only the
# first part of the list "full_list"
# remember you first have to work out half the list length and store it in a variable

def retrieve_second_half_of_list(list):
    list = ['One', 'Two', 'Three', 'Four']
    return
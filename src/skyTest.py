#  Could rewrite with try block, just use if/else
def assert_equals(function, expected):
    try:
        assert function == expected
        return '\033[32m' + "Success! Your assertion has passed!!! YAY, you rock!!" + '\033[0m'
    except AssertionError:
        return '\033[31m' + "Fail! Actual result " + "'" + str(function) + "'" + " does not equal expected " + "'" + str(expected) + "'" + '\033[0m'


def assert_true(function):
    try:
        assert function == True;
        return '\033[32m' + "Success! Your assertion has passed!!! YAY, you rock!!" + '\033[0m'
    except AssertionError:
        return '\033[31m' + "Fail! The argument returns 'False' when it should be 'True'" + '\033[0m'


def assert_false(function):
    if (function == False):
        return '\033[32m' + "Success! Your assertion has passed!!! YAY, you rock!!" + '\033[0m'
    else:
        return '\033[31m' + "Fail! The argument returns 'True' when it should be 'False'" + '\033[0m'

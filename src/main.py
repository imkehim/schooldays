import skyTest
import exercises

#--- starter Excercises ---
exercises_one = skyTest.assert_equals(exercises.hello_world(), "Hello World")
print("\nExercise One: Return the word Hello World " + "\n" + exercises_one)

exercises_two = skyTest.assert_equals(exercises.add_numbers(1, 2), 3)
print("\nExercise Two: Add the two numbers together and return the result " + "\n" + exercises_two)

exercises_three = skyTest.assert_equals(exercises.add_numbers(2, 3), 5)
print("\nExercise Three: Add the two numbers together and return the result " + "\n" + exercises_three)

exercises_four = skyTest.assert_equals(exercises.subtract_numbers(4, 2), 2)
print("\nExercise Four: Subtract the two numbers together and return the result " + "\n" + exercises_four)

exercises_five = skyTest.assert_equals(exercises.multiply_numbers(2, 8), 16)
print("\nExercise Five: Multiply two numbers together and return the result " + "\n" + exercises_five)

exercises_six = skyTest.assert_equals(exercises.divide_numbers(9, 3), 3)
print("\nExercise Six: Divide the two numbers together and return the result " + "\n" + exercises_six)

exercises_seven = skyTest.assert_equals(exercises.is_your_teacher_cool(True), "Woah you're soo lucky!")
print("\nExercise Seven: Return the message 'Woah you're soo lucky!' if your teacher is cool." + "\n" + exercises_seven)

exercises_eight = skyTest.assert_equals(exercises.is_your_teacher_cool(False), "Poor pudding... :(")
print("\nExercise Eight: Return the message 'Poor pudding... :(' if your teacher isn't cool." + "\n" + exercises_eight)

# This stuff is a bit harder:

exercises_nine = skyTest.assert_equals(exercises.tip_calculator(10), 2)
print("\nExercise Nine: Calculate and return the tip amount " + "\n" + exercises_nine)

exercises_ten = skyTest.assert_equals(exercises.first_word("Believe in Better"), "Believe")
print("\nExercise Ten: Return first word" + "\n" + exercises_ten)

exercises_eleven = skyTest.assert_equals(exercises.first_word("Hello World"), "Hello")
print("\nExercise Eleven: Return first word" + "\n" + exercises_eleven)

exercises_twelve = skyTest.assert_equals(exercises.find_middle_element(["David", "Wally", "Joshua"]), "Wally")
print("\nExercise Twelve: Return Wally from the list of names " + "\n" + exercises_twelve)

exercises_thirteen = skyTest.assert_equals(exercises.find_middle_element(["David", "Bob", "Joshua"]), "Bob")
print("\nExercise Thirteen: Return Bob from the list of names " + "\n" + exercises_thirteen)

exercises_fourteen = skyTest.assert_equals(exercises.retrieve_second_half_of_list(["One", "Two", "Three", "Four"]), ["Three", "Four"])
print("\nExercise Fourteen: Return the second half of elements in the list" + "\n" + exercises_fourteen)

exercises_fifteen = skyTest.assert_equals(exercises.retrieve_second_half_of_list(["A", "B", "C", "D"]), ["C", "D"])
print("\nExercise Fifteen: Return the second half of elements in the list" + "\n" + exercises_fifteen)
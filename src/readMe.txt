# --------- Introduction ---------
#   The exercises are split up into two sections:
# - In the first section you will be working with strings (words or sentences),
#   integers (whole numbers) and booleans (true or false).
# - In the second section you will be working with Arrays (lists of items)

# --------- Hints ---------
# - Think about what should be returned?
# - What is the easiest code to put in to pass the first failing test?
# - You should not solve the problem in one go, remember the requirements might change!
# - Just get one test working, the one you are working on, then focus on getting the next one
#   to pass (go green)

# --------- Pair Programming ---------
# - one of the pair partners uses the keyboard, the other helps to check code is correct
# - rotate who is on the keyboard
# - don't switch off when you're not typing code, your pairing partner still depends on you to help write good code

# --------- Exercises ---------
# - Make the exercise tests pass in 'main.py', the file where you will need to implement your code in is 'exercises.py'
# - Read the first test and work out what is required. Try and make it pass.
# - Run the tests (by running main.py) and read the error message. This will give you a clue about what you have to change in the
#   function that goes with each of the tests.